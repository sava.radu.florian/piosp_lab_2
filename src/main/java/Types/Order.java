package Types;

import java.util.Objects;

public class Order implements Comparable{
    private final int id;
    private final int price;
    private final int quantity;

    public Order(int id, int price, int quantity) {
        this.id = id;
        this.price = price;
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;
        Order order = (Order) o;
        return id == order.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }


    @Override
    public int compareTo(Object o) {
        if (this == o) return 0;
        if (!(o instanceof Order)) return 0;
        Order order = (Order) o;
        return this.price - order.price;
    }
}
