package Repos;

public interface InMemoryRepository<T> {

    void add(T e);
    boolean contains(T e);
    void remove(T e);
    int size();
}
