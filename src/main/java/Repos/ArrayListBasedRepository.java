package Repos;

import java.util.ArrayList;

public class ArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private ArrayList<T> list;

    public ArrayListBasedRepository() {list = new ArrayList<>();}

    @Override
    public void add(T e) {
        list.add(e);
    }

    @Override
    public boolean contains(T e) {
        return list.contains(e);
    }

    @Override
    public void remove(T e) {
        list.remove(e);
    }

    @Override
    public int size() {
        return list.size();
    }
}
