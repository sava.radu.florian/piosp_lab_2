package Repos;
import org.eclipse.collections.impl.map.mutable.ConcurrentHashMap;


public class EclipseConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T>{
    private ConcurrentHashMap<T, T> map;

    public EclipseConcurrentHashMapBasedRepository() {map = new ConcurrentHashMap<>();}

    @Override
    public void add(T e) {
        map.put(e, e);
    }

    @Override
    public boolean contains(T e) {
        return map.contains(e);
    }

    @Override
    public void remove(T e) {
        map.remove(e);
    }

    @Override
    public int size() {
        return map.size();
    }
}
