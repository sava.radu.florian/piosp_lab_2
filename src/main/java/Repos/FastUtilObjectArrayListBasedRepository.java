package Repos;

import it.unimi.dsi.fastutil.objects.ObjectArrayList;


public class FastUtilObjectArrayListBasedRepository<T> implements InMemoryRepository<T> {
    private ObjectArrayList<T> list;

    public FastUtilObjectArrayListBasedRepository() {list = new ObjectArrayList<>();}

    @Override
    public void add(T e) {
        list.add(e);
    }

    @Override
    public boolean contains(T e) {
        return list.contains(e);
    }

    @Override
    public void remove(T e) {
        list.remove(e);
    }

    @Override
    public int size() {
        return list.size();
    }
}
