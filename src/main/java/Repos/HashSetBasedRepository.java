package Repos;

import java.util.HashSet;

public class HashSetBasedRepository<T> implements InMemoryRepository<T> {
    private HashSet<T> set;

    public HashSetBasedRepository() {set = new HashSet<>();}

    @Override
    public void add(T e) {
        set.add(e);
    }

    @Override
    public boolean contains(T e) {
        return set.contains(e);
    }

    @Override
    public void remove(T e) {
        set.remove(e);
    }

    @Override
    public int size() {
        return set.size();
    }
}
