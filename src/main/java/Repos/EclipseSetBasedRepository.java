package Repos;

import org.eclipse.collections.impl.set.mutable.UnifiedSet;

public class EclipseSetBasedRepository<T> implements InMemoryRepository<T> {
    private UnifiedSet<T> set;

    public EclipseSetBasedRepository() {set = new UnifiedSet<>();}

    @Override
    public void add(T e) {
        set.add(e);
    }

    @Override
    public boolean contains(T e) {
        return set.contains(e);
    }

    @Override
    public void remove(T e) {
        set.remove(e);
    }

    @Override
    public int size() {
        return set.size();
    }
}
