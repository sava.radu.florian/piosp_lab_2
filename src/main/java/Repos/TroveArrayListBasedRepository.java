package Repos;

import gnu.trove.list.array.TIntArrayList;

public class TroveArrayListBasedRepository implements InMemoryRepository<Integer>{
    private TIntArrayList list;

    public TroveArrayListBasedRepository() { list = new TIntArrayList();}

    @Override
    public void add(Integer e) {
        list.add(e);
    }

    @Override
    public boolean contains(Integer e) {
        return list.contains(e);
    }

    @Override
    public void remove(Integer e) {
        list.remove(e);
    }

    @Override
    public int size() {
        return list.size();
    }
}
