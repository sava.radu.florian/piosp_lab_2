package Repos;

import java.util.TreeSet;

public class TreeSetBasedRepository<T> implements InMemoryRepository<T> {
    private TreeSet<T> tree;

    public TreeSetBasedRepository() {tree = new TreeSet<>();}

    @Override
    public void add(T e) {
        tree.add(e);
    }

    @Override
    public boolean contains(T e) {
        return tree.contains(e);
    }

    @Override
    public void remove(T e) {
        tree.remove(e);
    }

    @Override
    public int size() {
        return tree.size();
    }
}
