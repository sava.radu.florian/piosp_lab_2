package Repos;

import java.util.concurrent.ConcurrentHashMap;

public class ConcurrentHashMapBasedRepository<T> implements InMemoryRepository<T> {
    private ConcurrentHashMap<T,T> chm;

    public ConcurrentHashMapBasedRepository() {chm=new ConcurrentHashMap<>();}

    @Override
    public void add(T e) {
        chm.put(e, e);
    }

    @Override
    public boolean contains(T e) {
        return chm.contains(e);
    }

    @Override
    public void remove(T e) {
        chm.remove(e);
    }

    @Override
    public int size() {
        return chm.size();
    }
}
