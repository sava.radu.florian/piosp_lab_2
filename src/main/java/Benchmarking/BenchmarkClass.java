package Benchmarking;

//import Classes.*;
import Repos.*;
import Types.Order;
import org.openjdk.jmh.annotations.*;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

public class BenchmarkClass {
    private static final int forks = 1;
    private static final int warmups = 1;
    private static final int warmupIterations = 3;
    private static final int measurementIterations = 3;
    private static final int threads = 3;
    private static final int warmupTime = 60;
    private static final int measurementTime = 60;

    @State(Scope.Thread)
    public static class StateAdd {
        @Param({"1000", "20000"})
        public static int n;
        public StateAdd() {}
    }

    @State(Scope.Thread)
    public static class StateRemove {
        @Param({"1000", "20000"})
        public int n;
        public ArrayListBasedRepository<Order> arrayListBasedRepository = new ArrayListBasedRepository<>();
        public ConcurrentHashMapBasedRepository<Order> concurrentHashMapBasedRepository = new ConcurrentHashMapBasedRepository<>();
        public EclipseSetBasedRepository<Order> eclipseSetBasedRepository = new EclipseSetBasedRepository<>();
        public FastUtilObjectArrayListBasedRepository<Order> fastUtilObjectArrayListBasedRepository = new FastUtilObjectArrayListBasedRepository<>();
        public HashSetBasedRepository<Order> hashSetBasedRepository = new HashSetBasedRepository<>();
        public TreeSetBasedRepository<Order> treeSetBasedRepository = new TreeSetBasedRepository<>();
        public EclipseConcurrentHashMapBasedRepository<Order> eclipseConcurrentHashMapBasedRepository = new EclipseConcurrentHashMapBasedRepository<>();
        public TroveArrayListBasedRepository troveArrayListBasedRepository = new TroveArrayListBasedRepository();
        public StateRemove() {}
        @Setup(Level.Invocation)
        public void setup() {
            for (int i=0; i<n; i++) {
                arrayListBasedRepository.add(new Order(i, i, i));
                concurrentHashMapBasedRepository.add(new Order(i, i, i));
                eclipseSetBasedRepository.add(new Order(i, i, i));
                fastUtilObjectArrayListBasedRepository.add(new Order(i, i, i));
                hashSetBasedRepository.add(new Order(i, i, i));
                treeSetBasedRepository.add(new Order(i, i, i));
                eclipseConcurrentHashMapBasedRepository.add(new Order(i, i, i));
                troveArrayListBasedRepository.add(i);
            }
        }
    }

    @State(Scope.Thread)
    public static class StateContains {
        @Param({"1000", "20000"})
        public int n;
        public ArrayListBasedRepository<Order> arrayListBasedRepository = new ArrayListBasedRepository<>();
        public ConcurrentHashMapBasedRepository<Order> concurrentHashMapBasedRepository = new ConcurrentHashMapBasedRepository<>();
        public EclipseSetBasedRepository<Order> eclipseSetBasedRepository = new EclipseSetBasedRepository<>();
        public FastUtilObjectArrayListBasedRepository<Order> fastUtilObjectArrayListBasedRepository = new FastUtilObjectArrayListBasedRepository<>();
        public HashSetBasedRepository<Order> hashSetBasedRepository = new HashSetBasedRepository<>();
        public TreeSetBasedRepository<Order> treeSetBasedRepository = new TreeSetBasedRepository<>();
        public EclipseConcurrentHashMapBasedRepository<Order> eclipseConcurrentHashMapBasedRepository = new EclipseConcurrentHashMapBasedRepository<>();
        public TroveArrayListBasedRepository troveArrayListBasedRepository = new TroveArrayListBasedRepository();
        public StateContains() {}
        @Setup(Level.Invocation)
        public void setup() {
            for (int i=0; i<n; i++) {
                arrayListBasedRepository.add(new Order(i, i, i));
                concurrentHashMapBasedRepository.add(new Order(i, i, i));
                eclipseSetBasedRepository.add(new Order(i, i, i));
                fastUtilObjectArrayListBasedRepository.add(new Order(i, i, i));
                hashSetBasedRepository.add(new Order(i, i, i));
                treeSetBasedRepository.add(new Order(i, i, i));
                eclipseConcurrentHashMapBasedRepository.add(new Order(i, i, i));
                troveArrayListBasedRepository.add(i);
            }
        }
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void ADD_ArrayListBasedRepository(StateAdd state) throws Exception {
        ArrayListBasedRepository<Order> rep = new ArrayListBasedRepository<>();
        for (int i=0; i<state.n; i++) {
            rep.add(new Order(i, i, i));
        }
        checkSize(rep.size(), state.n);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void ADD_ConcurrentHashMapBasedRepository(StateAdd state) throws Exception {
        ConcurrentHashMapBasedRepository<Order> rep = new ConcurrentHashMapBasedRepository<>();
        for (int i=0; i<state.n; i++) {
            rep.add(new Order(i, i, i));
        }
        checkSize(rep.size(), state.n);

    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void ADD_EclipseSetBasedRepository(StateAdd state) throws Exception {
        EclipseSetBasedRepository<Order> rep = new EclipseSetBasedRepository<>();
        for (int i=0; i<state.n; i++) {
            rep.add(new Order(i, i, i));
        }
        checkSize(rep.size(), state.n);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void ADD_FastUtilObjectArrayListBasedRepository(StateAdd state) throws Exception {
        FastUtilObjectArrayListBasedRepository<Order> rep = new FastUtilObjectArrayListBasedRepository<>();
        for (int i=0; i<state.n; i++) {
            rep.add(new Order(i, i, i));
        }
        checkSize(rep.size(), state.n);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void ADD_HashSetBasedRepository(StateAdd state) throws Exception {
        HashSetBasedRepository<Order> rep = new HashSetBasedRepository<>();
        for (int i=0; i<state.n; i++) {
            rep.add(new Order(i, i, i));
        }
        checkSize(rep.size(), state.n);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void ADD_EclipseConcurrentHashMapBasedRepository(StateAdd state) throws Exception {
        EclipseConcurrentHashMapBasedRepository<Order> rep = new EclipseConcurrentHashMapBasedRepository<>();
        for (int i=0; i<state.n; i++) {
            rep.add(new Order(i, i, i));
        }
        checkSize(rep.size(), state.n);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void ADD_TreeSetBasedRepository(StateAdd state) throws Exception {
        TreeSetBasedRepository<Order> rep = new TreeSetBasedRepository<>();
        for (int i=0; i<state.n; i++) {
            rep.add(new Order(i, i, i));
        }
        checkSize(rep.size(), state.n);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void ADD_TroveArrayListBasedRepository(StateAdd state) throws Exception {
        TroveArrayListBasedRepository rep = new TroveArrayListBasedRepository();
        for (int i=0; i<state.n; i++) {
            rep.add(i);
        }
        checkSize(rep.size(), state.n);
    }


    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void REMOVE_ArrayListBasedRepository(StateRemove state) throws Exception {
        ArrayListBasedRepository<Order> rep = state.arrayListBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            rep.remove(new Order(i, i, i));
        }
        checkSize(rep.size(), 0);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void REMOVE_ConcurrentHashMapBasedRepository(StateRemove state) throws Exception {
        ConcurrentHashMapBasedRepository<Order> rep = state.concurrentHashMapBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            rep.remove(new Order(i, i, i));
        }
        checkSize(rep.size(), 0);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void REMOVE_EclipseSetBasedRepository(StateRemove state) throws Exception {
        EclipseSetBasedRepository<Order> rep = state.eclipseSetBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            rep.remove(new Order(i, i, i));
        }
        checkSize(rep.size(), 0);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void REMOVE_FastUtilObjectArrayListBasedRepository(StateRemove state) throws Exception {
        FastUtilObjectArrayListBasedRepository<Order> rep = state.fastUtilObjectArrayListBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            rep.remove(new Order(i, i, i));
        }
        checkSize(rep.size(), 0);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void REMOVE_HashSetBasedRepository(StateRemove state) throws Exception {
        HashSetBasedRepository<Order> rep = state.hashSetBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            rep.remove(new Order(i, i, i));
        }
        checkSize(rep.size(), 0);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void REMOVE_EclipseConcurrentHashMapBasedRepository(StateRemove state) throws Exception {
        EclipseConcurrentHashMapBasedRepository<Order> rep = state.eclipseConcurrentHashMapBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            rep.remove(new Order(i, i, i));
        }
        checkSize(rep.size(), 0);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void REMOVE_TreeSetBasedRepository(StateRemove state) throws Exception {
        TreeSetBasedRepository<Order> rep = state.treeSetBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            rep.remove(new Order(i, i, i));
        }
        checkSize(rep.size(), 0);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void REMOVE_TroveArrayListBasedRepository(StateRemove state) throws Exception {
        TroveArrayListBasedRepository rep = state.troveArrayListBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            rep.remove(i);
        }
        checkSize(rep.size(), 0);
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void CONTAINS_ArrayListBasedRepository(StateContains state) throws Exception {
        ArrayListBasedRepository<Order> rep = state.arrayListBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            checkTrue(rep.contains(new Order(i, i, i)));
        }
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void CONTAINS_ConcurrentHashMapBasedRepository(StateContains state) throws Exception {
        ConcurrentHashMapBasedRepository<Order> rep = state.concurrentHashMapBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            checkTrue(rep.contains(new Order(i, i, i)));
        }
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void CONTAINS_EclipseSetBasedRepository(StateContains state) throws Exception {
        EclipseSetBasedRepository<Order> rep = state.eclipseSetBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            checkTrue(rep.contains(new Order(i, i, i)));
        }
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void CONTAINS_FastUtilObjectArrayListBasedRepository(StateContains state) throws Exception {
        FastUtilObjectArrayListBasedRepository<Order> rep = state.fastUtilObjectArrayListBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            checkTrue(rep.contains(new Order(i, i, i)));
        }
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void CONTAINS_HashSetBasedRepository(StateContains state) throws Exception {
        HashSetBasedRepository<Order> rep = state.hashSetBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            checkTrue(rep.contains(new Order(i, i, i)));
        }
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void CONTAINS_EclipseConcurrentHashMapBasedRepository(StateContains state) throws Exception {
        EclipseConcurrentHashMapBasedRepository<Order> rep = state.eclipseConcurrentHashMapBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            checkTrue(rep.contains(new Order(i, i, i)));
        }
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void CONTAINS_TreeSetBasedRepository(StateContains state) throws Exception {
        TreeSetBasedRepository<Order> rep = state.treeSetBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            checkTrue(rep.contains(new Order(i, i, i)));
        }
    }

    @Benchmark
    @Fork(value = forks, warmups = warmups)
    @Measurement(iterations = measurementIterations, time = measurementTime, timeUnit = MILLISECONDS)
    @Warmup(iterations = warmupIterations, time = warmupTime, timeUnit = MILLISECONDS)
    @Threads(threads)
    public void CONTAINS_TroveArrayListBasedRepository(StateContains state) throws Exception {
        TroveArrayListBasedRepository rep = state.troveArrayListBasedRepository;
        checkSize(rep.size(), state.n);
        for (int i=0; i<state.n; i++) {
            checkTrue(rep.contains(i));
        }
    }

    private static void checkSize(int size, int check) throws Exception {
        if (size!=check) {
            throw new Exception("Value mismatch: ACTUAL SIZE=[" + size + "], EXPECTED SIZE=[" + check + "]");
        }
    }

    private static void checkTrue(boolean actual) throws Exception {
        if (!actual) {
            throw new Exception("FALSE");
        }
    }
}
